/*
*  Copyright 2019-2023 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package school.service.impl;

import school.domain.SchRegistratrion;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import school.service.SchRegistratrionService;
import school.domain.vo.SchRegistratrionQueryCriteria;
import school.mapper.SchRegistratrionMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import me.zhengjie.utils.PageUtil;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import me.zhengjie.utils.PageResult;

/**
* @description 服务实现
* @author zhangyu
* @date 2023-12-28
**/
@Service
@RequiredArgsConstructor
public class SchRegistratrionServiceImpl extends ServiceImpl<SchRegistratrionMapper, SchRegistratrion> implements SchRegistratrionService {

    private final SchRegistratrionMapper schRegistratrionMapper;

    @Override
    public PageResult<SchRegistratrion> queryAll(SchRegistratrionQueryCriteria criteria, Page<Object> page){
        return PageUtil.toPage(schRegistratrionMapper.findAll(criteria, page));
    }

    @Override
    public List<SchRegistratrion> queryAll(SchRegistratrionQueryCriteria criteria){
        return schRegistratrionMapper.findAll(criteria);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void create(SchRegistratrion resources) {
        save(resources);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(SchRegistratrion resources) {
        SchRegistratrion schRegistratrion = getById(resources.getRegId());
        schRegistratrion.copy(resources);
        saveOrUpdate(schRegistratrion);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteAll(List<Integer> ids) {
        removeBatchByIds(ids);
    }

    @Override
    public void download(List<SchRegistratrion> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (SchRegistratrion schRegistratrion : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("学生姓名", schRegistratrion.getStudentName());
            map.put("姓别", schRegistratrion.getStudentSex());
            map.put("学生出生日期", schRegistratrion.getStudentBirthday());
            map.put("学生国家", schRegistratrion.getStudentNation());
            map.put("学生籍贯", schRegistratrion.getStudentNativePlace());
            map.put("学生电话", schRegistratrion.getStudentPhone());
            map.put("学生邮箱", schRegistratrion.getStudentEmail());
            map.put("学生住址", schRegistratrion.getStudentAddr());
            map.put("监护人1姓名", schRegistratrion.getGuardianFirstName());
            map.put("监护人1关系", schRegistratrion.getGuardianFirstRelationship());
            map.put("监护人1电话", schRegistratrion.getGuardianFirstPhone());
            map.put("监护人1邮箱", schRegistratrion.getGuardianFirstEmail());
            map.put("监护人1住址", schRegistratrion.getGuardianFirstAddr());
            map.put("监护人2姓名", schRegistratrion.getGuardianSecondName());
            map.put("监护人2关系", schRegistratrion.getGuardianSecondRelationship());
            map.put("监护人2电话", schRegistratrion.getGuardianSecondPhone());
            map.put("监护人2邮箱", schRegistratrion.getGuardianSecondEmail());
            map.put("监护人2住址", schRegistratrion.getGuardianSecondAddr());
            map.put("申请状态", schRegistratrion.getState());
            map.put("逻辑删除", schRegistratrion.getDelFlag());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}