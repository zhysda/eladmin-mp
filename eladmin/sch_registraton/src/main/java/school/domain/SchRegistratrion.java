/*
*  Copyright 2019-2023 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package school.domain;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.bean.copier.CopyOptions;
import java.sql.Timestamp;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
* @description /
* @author zhangyu
* @date 2023-12-28
**/
@Data
@TableName("sch_registratrion")
public class SchRegistratrion implements Serializable {

    @TableId(value = "reg_id", type = IdType.AUTO)
    @ApiModelProperty(value = "regId")
    private Integer regId;

    @NotBlank
    @ApiModelProperty(value = "学生姓名")
    private String studentName;

    @NotNull
    @ApiModelProperty(value = "姓别")
    private Integer studentSex;

    @NotNull
    @ApiModelProperty(value = "学生出生日期")
    private Timestamp studentBirthday;

    @ApiModelProperty(value = "学生国家")
    private String studentNation;

    @ApiModelProperty(value = "学生籍贯")
    private String studentNativePlace;

    @ApiModelProperty(value = "学生电话")
    private String studentPhone;

    @ApiModelProperty(value = "学生邮箱")
    private String studentEmail;

    @ApiModelProperty(value = "学生住址")
    private String studentAddr;

    @ApiModelProperty(value = "监护人1姓名")
    private String guardianFirstName;

    @ApiModelProperty(value = "监护人1关系")
    private String guardianFirstRelationship;

    @ApiModelProperty(value = "监护人1电话")
    private String guardianFirstPhone;

    @ApiModelProperty(value = "监护人1邮箱")
    private String guardianFirstEmail;

    @ApiModelProperty(value = "监护人1住址")
    private String guardianFirstAddr;

    @ApiModelProperty(value = "监护人2姓名")
    private String guardianSecondName;

    @ApiModelProperty(value = "监护人2关系")
    private String guardianSecondRelationship;

    @ApiModelProperty(value = "监护人2电话")
    private String guardianSecondPhone;

    @ApiModelProperty(value = "监护人2邮箱")
    private String guardianSecondEmail;

    @ApiModelProperty(value = "监护人2住址")
    private String guardianSecondAddr;

    @ApiModelProperty(value = "申请状态")
    private Integer state;

    @ApiModelProperty(value = "逻辑删除")
    private Integer delFlag;

    public void copy(SchRegistratrion source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}
