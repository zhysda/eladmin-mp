/*
*  Copyright 2019-2023 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package school.rest;

import me.zhengjie.annotation.Log;
import school.domain.SchRegistratrion;
import school.service.SchRegistratrionService;
import school.domain.vo.SchRegistratrionQueryCriteria;
import lombok.RequiredArgsConstructor;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import me.zhengjie.utils.PageResult;

/**
* @author zhangyu
* @date 2023-12-28
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "入学申请管理")
@RequestMapping("/api/schRegistratrion")
public class SchRegistratrionController {

    private final SchRegistratrionService schRegistratrionService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('schRegistratrion:list')")
    public void exportSchRegistratrion(HttpServletResponse response, SchRegistratrionQueryCriteria criteria) throws IOException {
        schRegistratrionService.download(schRegistratrionService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询入学申请")
    @ApiOperation("查询入学申请")
    @PreAuthorize("@el.check('schRegistratrion:list')")
    public ResponseEntity<PageResult<SchRegistratrion>> querySchRegistratrion(SchRegistratrionQueryCriteria criteria, Page<Object> page){
        return new ResponseEntity<>(schRegistratrionService.queryAll(criteria,page),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增入学申请")
    @ApiOperation("新增入学申请")
    @PreAuthorize("@el.check('schRegistratrion:add')")
    public ResponseEntity<Object> createSchRegistratrion(@Validated @RequestBody SchRegistratrion resources){
        schRegistratrionService.create(resources);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改入学申请")
    @ApiOperation("修改入学申请")
    @PreAuthorize("@el.check('schRegistratrion:edit')")
    public ResponseEntity<Object> updateSchRegistratrion(@Validated @RequestBody SchRegistratrion resources){
        schRegistratrionService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping
    @Log("删除入学申请")
    @ApiOperation("删除入学申请")
    @PreAuthorize("@el.check('schRegistratrion:del')")
    public ResponseEntity<Object> deleteSchRegistratrion(@RequestBody List<Integer> ids) {
        schRegistratrionService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}