/*
*  Copyright 2019-2023 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package cms.service.impl;

import cms.domain.CmsArticle;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cms.service.CmsArticleService;
import cms.domain.vo.CmsArticleQueryCriteria;
import cms.mapper.CmsArticleMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import me.zhengjie.utils.PageUtil;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import me.zhengjie.utils.PageResult;

/**
* @description 服务实现
* @author zhangyu
* @date 2023-12-28
**/
@Service
@RequiredArgsConstructor
public class CmsArticleServiceImpl extends ServiceImpl<CmsArticleMapper, CmsArticle> implements CmsArticleService {

    private final CmsArticleMapper cmsArticleMapper;

    @Override
    public PageResult<CmsArticle> queryAll(CmsArticleQueryCriteria criteria, Page<Object> page){
        return PageUtil.toPage(cmsArticleMapper.findAll(criteria, page));
    }

    @Override
    public List<CmsArticle> queryAll(CmsArticleQueryCriteria criteria){
        return cmsArticleMapper.findAll(criteria);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void create(CmsArticle resources) {
        save(resources);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(CmsArticle resources) {
        CmsArticle cmsArticle = getById(resources.getArticleId());
        cmsArticle.copy(resources);
        saveOrUpdate(cmsArticle);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteAll(List<Integer> ids) {
        removeBatchByIds(ids);
    }

    @Override
    public void download(List<CmsArticle> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (CmsArticle cmsArticle : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("文章标题", cmsArticle.getTitle());
            map.put("分类ID", cmsArticle.getCateId());
            map.put("简介", cmsArticle.getDescription());
            map.put("文章内容", cmsArticle.getContent());
            map.put("封面", cmsArticle.getCoverImg());
            map.put("封面小图", cmsArticle.getCoverImgSmall());
            map.put("作者", cmsArticle.getAuthor());
            map.put("发布时间", cmsArticle.getPublicDate());
            map.put("来源", cmsArticle.getSource());
            map.put("排序", cmsArticle.getSort());
            map.put("置顶", cmsArticle.getTop());
            map.put("逻辑删除", cmsArticle.getDelFlag());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}