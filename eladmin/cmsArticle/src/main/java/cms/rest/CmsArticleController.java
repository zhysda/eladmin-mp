/*
*  Copyright 2019-2023 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package cms.rest;

import me.zhengjie.annotation.Log;
import cms.domain.CmsArticle;
import cms.service.CmsArticleService;
import cms.domain.vo.CmsArticleQueryCriteria;
import lombok.RequiredArgsConstructor;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import me.zhengjie.utils.PageResult;

/**
* @author zhangyu
* @date 2023-12-28
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "文章管理")
@RequestMapping("/api/cmsArticle")
public class CmsArticleController {

    private final CmsArticleService cmsArticleService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('cmsArticle:list')")
    public void exportCmsArticle(HttpServletResponse response, CmsArticleQueryCriteria criteria) throws IOException {
        cmsArticleService.download(cmsArticleService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询文章")
    @ApiOperation("查询文章")
    @PreAuthorize("@el.check('cmsArticle:list')")
    public ResponseEntity<PageResult<CmsArticle>> queryCmsArticle(CmsArticleQueryCriteria criteria, Page<Object> page){
        return new ResponseEntity<>(cmsArticleService.queryAll(criteria,page),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增文章")
    @ApiOperation("新增文章")
    @PreAuthorize("@el.check('cmsArticle:add')")
    public ResponseEntity<Object> createCmsArticle(@Validated @RequestBody CmsArticle resources){
        cmsArticleService.create(resources);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改文章")
    @ApiOperation("修改文章")
    @PreAuthorize("@el.check('cmsArticle:edit')")
    public ResponseEntity<Object> updateCmsArticle(@Validated @RequestBody CmsArticle resources){
        cmsArticleService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping
    @Log("删除文章")
    @ApiOperation("删除文章")
    @PreAuthorize("@el.check('cmsArticle:del')")
    public ResponseEntity<Object> deleteCmsArticle(@RequestBody List<Integer> ids) {
        cmsArticleService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}