/*
*  Copyright 2019-2023 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package cms.domain;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.bean.copier.CopyOptions;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
* @description /
* @author zhangyu
* @date 2023-12-28
**/
@Data
@TableName("cms_category")
public class CmsCategory implements Serializable {

    @TableId(value = "cate_id", type = IdType.AUTO)
    @ApiModelProperty(value = "cateId")
    private Integer cateId;

    @NotBlank
    @ApiModelProperty(value = "name")
    private String name;

    @ApiModelProperty(value = "parentCateId")
    private Integer parentCateId;

    @ApiModelProperty(value = "remark")
    private String remark;

    @NotNull
    @ApiModelProperty(value = "delFlag")
    private Integer delFlag;

    public void copy(CmsCategory source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}
