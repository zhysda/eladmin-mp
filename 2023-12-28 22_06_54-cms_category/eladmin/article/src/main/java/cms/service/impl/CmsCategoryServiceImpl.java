/*
*  Copyright 2019-2023 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package cms.service.impl;

import cms.domain.CmsCategory;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cms.service.CmsCategoryService;
import cms.domain.vo.CmsCategoryQueryCriteria;
import cms.mapper.CmsCategoryMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import me.zhengjie.utils.PageUtil;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import me.zhengjie.utils.PageResult;

/**
* @description 服务实现
* @author zhangyu
* @date 2023-12-28
**/
@Service
@RequiredArgsConstructor
public class CmsCategoryServiceImpl extends ServiceImpl<CmsCategoryMapper, CmsCategory> implements CmsCategoryService {

    private final CmsCategoryMapper cmsCategoryMapper;

    @Override
    public PageResult<CmsCategory> queryAll(CmsCategoryQueryCriteria criteria, Page<Object> page){
        return PageUtil.toPage(cmsCategoryMapper.findAll(criteria, page));
    }

    @Override
    public List<CmsCategory> queryAll(CmsCategoryQueryCriteria criteria){
        return cmsCategoryMapper.findAll(criteria);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void create(CmsCategory resources) {
        save(resources);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(CmsCategory resources) {
        CmsCategory cmsCategory = getById(resources.getCateId());
        cmsCategory.copy(resources);
        saveOrUpdate(cmsCategory);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteAll(List<Integer> ids) {
        removeBatchByIds(ids);
    }

    @Override
    public void download(List<CmsCategory> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (CmsCategory cmsCategory : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put(" name",  cmsCategory.getName());
            map.put(" parentCateId",  cmsCategory.getParentCateId());
            map.put(" remark",  cmsCategory.getRemark());
            map.put(" delFlag",  cmsCategory.getDelFlag());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}